import * as https from "https";

export const BASE_URL: string = "https://my.ukma.edu.ua";
//ця стрічка потрібна, щоб обійти проблеми з сертифікатом САЗу - "unable to verify the first certificate"
export const httpsAgent = new https.Agent({rejectUnauthorized: false});
