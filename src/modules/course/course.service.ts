import {Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import {ICourse} from "./types";
import {CourseSeason, EducationLevel} from "../../common/types";
import {BASE_URL, httpsAgent} from "../../common/constants";
import axios from 'axios'
import * as cheerio from 'cheerio'

@Injectable()
export class CourseService {
    public async getCourse(code: number): Promise<ICourse> {

        const response = await axios.get(`${BASE_URL}/course/${code}`, {httpsAgent})
            .catch(err => {
                if (err.response.status === 404) throw new NotFoundException("Page not found");
                throw new InternalServerErrorException();
            });

        const data = await response.data;
        return this.parseCourse(data);
    }

    private parseCourse(data: string): ICourse {
        const $ = cheerio.load(data);

        const code: number = +$(this.getField(1, 1)).text();
        const name: string = $(".active").text();
        const desc: string =
            $(`course-card--${code}--info`).text().trim()
        const description = desc !== "" ? undefined : desc
        const facultyName: string = $(this.getField(1, 3)).text();

        const level: EducationLevel = this.mapStringToLevel($(this.getField(1, 5)).text());
        const year: 1 | 2 | 3 | 4 = +$(`${this.getField(1, 2)} > span:nth-child(3)`).text().split(" ")[0] as 1 | 2 | 3 | 4;
        const seasons: CourseSeason[] = $(`${this.getTable(2)} > tr > th`).slice(2)
            .map((i, row) => this.mapStringToSeason($(row).text().trim())).get()

        const credits = +$(`${this.getField(1, 2)} > span:nth-child(1)`).text().split(" ")[0];
        const hours = +$(`${this.getField(1, 2)} > span:nth-child(2)`).text().split(" ")[0];
        const teacherName =
            $(`${this.getTable(2)} > tr:nth-child(7) > th`).text() === 'Викладач'
                ? $(`${this.getTable(2)} > tr:nth-child(7) > td`).text().trim()
                : undefined
        const departmentName: string = $(this.getField(1, 4)).text();
        return {
            code: code,
            name: name,
            description: description,
            facultyName: facultyName,
            departmentName: departmentName,
            level: level,
            year: year,
            seasons: seasons,
            creditsAmount: credits,
            hoursAmount: hours,
            teacherName: teacherName,
        };
    }

    private getTable(tbodyId: number): string {
        return `#w0 > table > tbody:nth-child(${tbodyId})`;
    }

    private getTr(trId: number): string {
        return `tr:nth-child(${trId})`;
    }

    private getTd(tdId: number): string {
        return `td:nth-child(${tdId})`;
    }

    private getField(tbodyId: number, trId: number, tdId: number = 2) {
        return `${this.getTable(tbodyId)} > ${this.getTr(trId)} > ${this.getTd(tdId)}`
    }

    private mapStringToLevel(level: string): EducationLevel {
        if (level === 'Бакалавр') return EducationLevel.BACHELOR;
        return EducationLevel.MASTER;
    }

    private mapStringToSeason(season: string): CourseSeason {
        if (season === 'Осінь') return CourseSeason.AUTUMN
        if (season === 'Весна') return CourseSeason.SPRING
        return CourseSeason.SUMMER
    }
}

