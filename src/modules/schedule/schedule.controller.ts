import {
    BadRequestException,
    Controller,
    Get,
    InternalServerErrorException,
    NotFoundException,
    Param
} from '@nestjs/common';
import {ScheduleService} from './schedule.service';
import {IScheduleItem} from "./types";

@Controller('schedule')
export class ScheduleController {
    constructor(
        protected readonly service: ScheduleService,
    ) {
    }

    private seasons = ['spring', 'autumn', 'summer']

    @Get(':year/:season')
    public async getSchedule(@Param('year') year: number,
                             @Param('season') season: string): Promise<IScheduleItem[]> {
        if (!this.seasons.includes(season)) {
            throw new BadRequestException(`Season ${season} is of impossible value`)
        }
        return await this.service.getSchedule(year, season)
            .then(data => {
                if (!data || data.length === 0) throw new NotFoundException("No data found")
                return data
            }).catch(err => {
                const status = err?.response?.status
                if (err.message === 'No data found') throw new NotFoundException("No data found")
                if (status == 404) throw new NotFoundException(`No data found`);
                throw new InternalServerErrorException("Error")
            })
    }
}
