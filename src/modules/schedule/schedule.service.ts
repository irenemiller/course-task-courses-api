import {Injectable, InternalServerErrorException, NotFoundException} from '@nestjs/common';
import {IScheduleItem} from "./types";
import {CourseSeason, EducationLevel} from "../../common/types";
import {BASE_URL, httpsAgent} from "../../common/constants";
import axios from "axios";
import * as cheerio from 'cheerio'

@Injectable()
export class ScheduleService {

    public async getSchedule(year: number, season: string): Promise<IScheduleItem[]> {
        let realYear = year
        let seasonNumber: number
        let courseSeason: CourseSeason
        if (season === 'autumn') {
            seasonNumber = 1;
            courseSeason = CourseSeason.AUTUMN;
        } else if (season === 'spring') {
            seasonNumber = 2;
            courseSeason = CourseSeason.SPRING
            realYear--;
        } else {
            seasonNumber = 3;
            courseSeason = CourseSeason.SUMMER
            realYear--;
        }
        const response = await axios.get(`${BASE_URL}/schedule/?year=${realYear}&season=${seasonNumber}`, {httpsAgent})
            .catch(err => {
                if (err.response.status === 404) throw new NotFoundException("Page not found on API side");
                throw new InternalServerErrorException();
            });

        const data = await response.data
        return this.parsePage(data, courseSeason);
    }

    private parsePage(data: string, courseSeason: CourseSeason): IScheduleItem[] {
        //факультет, рівень, рік, спеціальність, сезон
        const $ = cheerio.load(data)
        const schedules: IScheduleItem[] = [];
        $(`#schedule-accordion > div`).each((i, panel) => {
            const faculty = $(panel).find("div").eq(0).find("a").text().trim();
            const facultyBody = $(panel).find("div").eq(1)
                .find("div[id^='schedule-faculty'] > div");

            facultyBody.each((id, el) => {
                const header = $(el).find("a").text().trim().split(", ");
                const year: number = +header[1].split(" ")[0];

                const scheduleBody = $(el).find("div[id^='schedule-faculty'] li > div");
                scheduleBody.each((idx, el) => {
                    const current = $(el).find("a[title='Завантажити']").not("[target='_blank']");
                    const datetime = $(el).find("span").text().trim().split(" ");
                    const date = datetime[1].split(".").reverse().join("-");
                    const time = datetime[2].substring(0, datetime[2].length - 1);
                    schedules.push({
                        url: current.attr('href') || "",
                        updatedAt: `${date} ${time}`,
                        facultyName: faculty,
                        specialityName:  current.text().trim(),
                        level: this.parseLevel(header[0]),
                        year: year as 1 | 2 | 3 | 4,
                        season: courseSeason
                    });
                });
            });
        });
        return schedules
    }

    private parseLevel(level: string): EducationLevel {
        if (level == 'БП') return EducationLevel.BACHELOR
        return EducationLevel.MASTER
    }
}
